
<center><h1>Bienvenue sur le site de la Maison des Ligues</h1></center><br>
<p style="text-align:justify;">La MDL est un organisme qui fournit aux différentes associations sportives adhérentes un
soutien logistique dans leur gestion.
Elle met notamment à disposition des associations un ensemble de salles de travail et permet
de mutualiser du personnel administratif (une secrétaire peut travailler pour plusieurs
associations qui n’auraient pas les moyens de payer seules son salaire).
<br>La MDL fournit également une équipe informatique payée par des subventions régionales afin
de mettre en place des services informatiques relevant aussi bien du domaine du réseau que
du développement. Cette équipe travaille seule sur certains projets mais peut être amenée à
faire travailler des entreprises prestataires pour des besoins ponctuels.
<br>
La MDL s’est vue confiée une nouvelle mission pour le compte de l’association « Le Domaine
du verger ». Située au coeur de vergers de mirabelliers en Lorraine, cette association effectue
plusieurs activités :<ul>
<li> Des compétitions sportives canines
<li> Des cours d’obéissance canine
<li> Un refuge pour chiens abandonnés</ul>
L’association créée en 1975 par Bill Dogue, un américain né au Texas, a été récemment reprise
par une équipe dynamique qui souhaite placer l’usage des NTIC au coeur de son activité. Il y a
du travail car jusqu’à présent, les membres utilisaient très peu l’outil informatique dans la
gestion quotidienne des activités.
Le nouveau président de l’association, Jean-Luc Fox, a contacté la MDL pour l’aider à réaliser
plusieurs projets.
</p>