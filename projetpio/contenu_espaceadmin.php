<table style="margin-left:75px; margin-right:75px;">
	<tr>
		<td width="225">
		<?php
		if ($_SESSION['admin']=="" || $_SESSION['admin']=="0")
		{
			header ('Location: http://172.17.0.4/projetpio/index.php?uc=connexion');
		}
		if(!file_exists("avatar/".$_SESSION['id'].".jpg"))
		{
			echo "<img src='avatar/default.jpg' width='175' height='175' align='left'>";
		}
		else
		{
			echo "<img src='avatar/".$_SESSION['id'].".jpg' width='175' height='175' align='left'>";
		}
		?>
		</td>
		<td>
			<h1><?php echo strtoupper($_SESSION['nom'])." ".strtoupper(substr($_SESSION['prenom'],0,1)).substr($_SESSION['prenom'],1);?></h1>
			Date de naissance : <?php echo substr($_SESSION['datenaiss'],8,2)."/".substr($_SESSION['datenaiss'],5,2)."/".substr($_SESSION['datenaiss'],0,4);?><br>
			Adresse : <?php echo $_SESSION['adresse']?><br>
			Numéro de téléphone : <?php echo substr($_SESSION['tel'],0,2)." ".substr($_SESSION['tel'],2,2)." ".substr($_SESSION['tel'],4,2)." ".substr($_SESSION['tel'],6,2)." ".substr($_SESSION['tel'],8,2);?><br>
			Adresse e-mail : <?php echo $_SESSION['email']?><br><br>
			Date du renouvellement : <?php echo substr($_SESSION['datedernierpaiement'],8,2)."/".substr($_SESSION['datedernierpaiement'],5,2)."/".substr($_SESSION['datedernierpaiement'],0,4);?><br>
			
		</td>
	</tr>
	<?php
	if(!file_exists("avatar/".$_SESSION['id'].".jpg"))
	{
		echo "<tr><td height='50'>Ajouter un avatar : </td><td colspan=10 valign='bottom'><form method='POST' enctype='multipart/form-data' action='http://172.17.0.4/projetpio/index.php?uc=ajoutAvatar'><br><input type='file' name='avatar'> <input type='submit' name='nvavatar' value='Ajouter cet avatar'></form></td></tr>";
	}
	?>
</table>
<br>
<h2>Voici les demandes de contact :</h2><br>
<center>
<table style="font-size:20px;" border=1>
<tr bgcolor="#404040" style="color:white; text-shadow: #000000 1px 1px, #000000 -1px 1px, #000000 -1px -1px, #000000 1px -1px;" align='center' valign='middle'><td>NOM</td><td>PRÉNOM</td><td>DATE DE NAISSANCE</td><td>ADRESSE</td><td>NUMÉRO DE TÉLÉPHONE</td><td>ADRESSE E-MAIL</td><td>PAPIERS JOINTS</td><td>AUTORISÉ</td><td>DÉCISION</td></tr>
<?php
$test = $pdo->affichedemandeurs();
$col=0;
for($i=0;$i<count($test);$i++)
{
	if($col%2==0)
	{
		echo "<tr bgcolor='white' align='center' valign='middle'>";
	}
	else
	{
		echo "<tr bgcolor='#C2C3C3' align='center' valign='middle'>";
	}
	for($j=1;$j<9;$j++)
	{
		if($j==3)
		{
			echo "<td style='font-size:20px;'> ".substr($test[$i][3],8,2)."/".substr($test[$i][3],5,2)."/".substr($test[$i][3],0,4)."</td>";
		}
		else if($j!=3)
		{
			echo "<td style='font-size:20px;'> ".$test[$i][$j]."</td>";
		}
	}
	echo "<td><br><form method='POST' action='http://172.17.0.4/projetpio/index.php?uc=acceptation'><input type='submit' name='yes[".$test[$i][0]."£".$test[$i][1]."£".$test[$i][2]."£".$test[$i][6]."]' id='".$test[$i][0].$test[$i][1].$test[$i][2].$test[$i][6]."' value='Accepté'></form><form method='POST' action='http://172.17.0.4/projetpio/index.php?uc=refus'><input type='submit' name='no[".$test[$i][0]."£".$test[$i][1]."£".$test[$i][2]."£".$test[$i][6]."]' id='".$test[$i][0].$test[$i][1]."£".$test[$i][2]."£".$test[$i][6]."' value='Refusé'></td></form>";
	echo "</tr>";
	$col++;
}
?>
</table>

</center>

<br>
<h2>Voici les inscrits non à jour :</h2><br>
<center>
<table style="font-size:20px;" border=1>
<tr bgcolor="#404040" style="color:white; text-shadow: #000000 1px 1px, #000000 -1px 1px, #000000 -1px -1px, #000000 1px -1px;" align='center' valign='middle'><td>NOM</td><td>PRÉNOM</td><td>ADRESSE</td><td>NUMÉRO DE TÉLÉPHONE</td><td>ADRESSE E-MAIL</td><td>DATE DERNIER PAIEMENT</td><td>CHANGEMENT</td></tr>
<?php
$test2 = $pdo->afficheinscrits();
$col2=0;
for($i=0;$i<count($test2);$i++)
{
	if($col2%2==0)
	{
		echo "<tr bgcolor='white' align='center' valign='middle'>";
	}
	else
	{
		echo "<tr bgcolor='#C2C3C3' align='center' valign='middle'>";
	}
	for($j=0;$j<6;$j++)
	{
		if($j==5)
		{
			echo "<td style='font-size:20px;'> ".substr($test2[$i][5],8,2)."/".substr($test2[$i][5],5,2)."/".substr($test2[$i][5],0,4)."</td>";
		}
		else if($j!=5)
		{
			echo "<td style='font-size:20px;'> ".$test2[$i][$j]."</td>";
		}
	}
	echo "<td><br><form method='POST' action='http://172.17.0.4/projetpio/index.php?uc=ajour'><input type='submit' name='ajour[".$test2[$i][0]."]' id='".$test2[$i][0]."' value='Est à jour'></form></td>";
	echo "</tr>";
	$col2++;
}
?>
</table>

</center>