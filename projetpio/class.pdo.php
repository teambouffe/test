<?php
class pdopio
{   		
      	private static $serveur='mysql:host=127.0.0.1';
      	private static $bdd='dbname=pio';   		
      	private static $user='root' ;    		
      	private static $mdp='btssio' ;	
		private static $monpdo;
		private static $monpdopio = null;
		
		
		private function __construct()
	{
    		pdopio::$monpdo = new PDO(pdopio::$serveur.';'.pdopio::$bdd, pdopio::$user, pdopio::$mdp); 
			pdopio::$monpdo->query("SET CHARACTER SET utf8");
	}
	
		public function __destruct()
	{
		pdopio::$monpdo = null;
	}
	
	public  static function getpdopio()
	{
		if(pdopio::$monpdopio == null)
		{
			pdopio::$monpdopio= new pdopio();
		}
		return pdopio::$monpdopio;  
	}
	
		public function demandeinscription($nom,$prenom,$datenaiss,$adresse,$tel,$email,$majeur)
	{
		$req="insert into demandeurs values(null,'".$nom."','".$prenom."','".$datenaiss."','".$adresse."','".$tel."','".$email."','0','".$majeur."')";
		$res=pdopio::$monpdo->query($req);
	}
	
	public function connection($login,$mdp)
	{
		$req="select * from inscrit where login='".$login."' and mdp='".$mdp."'";
		$req2="update inscrit set estConnecte =1 where login='".$login."'and mdp='".$mdp."'";
		$res=pdopio::$monpdo->query($req);
		$res2=pdopio::$monpdo->query($req2);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}

	public function adminConnecte(){
		
		$req = "SELECT COUNT(id) FROM inscrit WHERE admin='1' AND estConnecte='1'";
		$res=pdopio::$monpdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
		
	}
	
	public function affichemessage()
	{	
		if(!isset($_SESSION["id"])){
			
			$_SESSION["id"] = rand(1000,5000);
		}		
		$req="select (select login from inscrit where id=id_user), message from tchat where id_user=".$_SESSION["id"]." limit 20";
		$res=pdopio::$monpdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes;
		
	}
		
		
		
		
	
	
	public function deconnection()
	{
		$req = "update inscrit set estConnecte =0 where id='".$_SESSION['id']."'";
		$res = pdopio::$monpdo->query($req);
	}
	
	public function affichedemandeurs()
	{
		$req="select * from demandeurs";
		$res=pdopio::$monpdo->query($req); 	
		$lesLignes = $res->fetchAll();
		return $lesLignes;		
	}
	
	
	public function suppressiondemandeurs($id)
	{
		$req="delete * from demandeurs where id='".$id."'";
        $res=pdopio::$monpdo->query($req);		
	}
		
	public function inscriptionchien($id)
	{
	  $req=" insert into chiens values(".$id.",'".$_POST['nomchien']."','".$_POST['race']."',".str_replace("-","",$_POST['datenaisschien']).",".$_POST['tatouage'].",".$_POST['lof'].",".date("Ymd").",".date("Ymd").")";
	  $res=pdopio::$monpdo->query($req);
	}
	
	public function inscriptionfamille($id)
	{
	  $req = "insert into famille values (".$id.", '".$_POST['nom']."', '".$_POST['prenom']."', '".str_replace("-","",$_POST['date'])."', '".$_POST['adresse']."', '".$_POST['tel']."', '".$_POST['email']."')";
      $res=pdopio::$monpdo->query($req);
	}

	public function envoiMailOui($nom, $prenom, $email)
	{
		$login = strtolower(substr($prenom,0,1).$nom);
		
		$caract="azertyuiopqsdfghjklmwxcvbn1234567890@+-#!=*";
		$cpt=0;
		$mdp="";
		while($cpt!=8)
		{
			$nb = rand(0,44);
			$mdp.=substr($caract, $nb, 1);
			$cpt++;
		}
		
		include('mailOui.php');
		$v = $login."£".$mdp;
		return $v;
	}
	
	public function envoiMailNon($nom, $prenom, $email)
	{
		include('mailNon.php');
	}
	
	public function recupDonnees($prop)
	{
		$req = "select * from demandeurs where id=".intval($prop)."";
		$res=pdopio::$monpdo->query($req);
		return $res->fetchAll();
	}
	
	public function lesChiens()
	{
		$req = "select * from race";
		$res=pdopio::$monpdo->query($req);
		return $res->fetchAll();
	}
	
	public function mesChiens($id)
	{
		$req = "select * from chiens where id=".$id."";
		$res=pdopio::$monpdo->query($req);
		return $res->fetchAll();
	}
	
	public function laFamille()
	{
		$req = "select * from famille where id=".$_SESSION['id']."";
		$res=pdopio::$monpdo->query($req);
		return $res->fetchAll();
	}
	
	public function transfertDonnees($id, $log, $mdp, $nom, $prenom, $date, $adresse, $tel, $email)
	{
		$req = "insert into inscrit values (null, '".$log."', '".$mdp."', '".$nom."', '".$prenom."', '".$date."', '".$adresse."', '".$tel."', '".$email."', 0, ".date("Ymd").", ".date("Ymd").", ".$id." , 'onverra')";
        $res=pdopio::$monpdo->query($req);
		$req2 = "delete from demandeurs where id=".$id."";
		$res2=pdopio::$monpdo->query($req2);
	}
	
	public function degage($id)
	{
		$req = "delete from demandeurs where id=".$id."";
		$res=pdopio::$monpdo->query($req);
	}
	
	public function ajoutAvatar($id)
	{
		$req = "update inscrit set avatar='".$id."' where id=".$id."";
		$res=pdopio::$monpdo->query($req);
	}
	
	public function afficheinscrits()
	{
		$req="select nom, prenom, adresse, tel, email, datedernierpaiement, datediff(NOW(), datedernierpaiement) as jours from inscrit having jours > 365";
		$res=pdopio::$monpdo->query($req); 	
		$lesLignes = $res->fetchAll();
		return $lesLignes;
	}
	
	public function ajour($id)
	{
		$req = "update inscrit set datedernierpaiement='".date('Ymd')."' where id=".$id."";
		$res=pdopio::$monpdo->query($req);
	}
}
?>
