<table style="margin-left:75px; margin-right:75px;">
	<tr>
		<td width="225">
		<?php
		if(!file_exists("avatar/".$_SESSION['id'].".jpg"))
		{
			echo "<img src='avatar/default.jpg' width='175' height='175' align='left'>";
		}
		else
		{
			echo "<img src='avatar/".$_SESSION['id'].".jpg' width='175' height='175' align='left'>";
		}
		?>
		</td>
		<td>
			<h1><?php echo strtoupper($_SESSION['nom'])." ".strtoupper(substr($_SESSION['prenom'],0,1)).substr($_SESSION['prenom'],1);?></h1>
			Date de naissance : <?php echo substr($_SESSION['datenaiss'],8,2)."/".substr($_SESSION['datenaiss'],5,2)."/".substr($_SESSION['datenaiss'],0,4);?><br>
			Adresse : <?php echo $_SESSION['adresse']?><br>
			Numéro de téléphone : <?php echo substr($_SESSION['tel'],0,2)." ".substr($_SESSION['tel'],2,2)." ".substr($_SESSION['tel'],4,2)." ".substr($_SESSION['tel'],6,2)." ".substr($_SESSION['tel'],8,2);?><br>
			Adresse e-mail : <?php echo $_SESSION['email']?><br><br>
			Date du renouvellement : <?php echo substr($_SESSION['datedernierpaiement'],8,2)."/".substr($_SESSION['datedernierpaiement'],5,2)."/".substr($_SESSION['datedernierpaiement'],0,4);?><br>
			
		</td>
	</tr>
	<?php
	if(!file_exists("avatar/".$_SESSION['id'].".jpg"))
	{
		echo "<tr><td height='50'>Ajouter un avatar : </td><td colspan=10 valign='bottom'><form method='POST' enctype='multipart/form-data' action='http://172.17.0.4/projetpio/index.php?uc=ajoutAvatar'><br><input type='file' name='avatar'> <input type='submit' name='nvavatar' value='Ajouter cet avatar'></form></td></tr>";
	}
	?>
	<tr>
		<td width="225" height="100">Membres de la familles associés : </td><td><?php 
					$u = $pdo->laFamille();
					for ($i=0;$i<count($u);$i++)
					{
						echo "".strtoupper($u[$i][1])." ".strtoupper(substr($u[$i][2],0,1)).substr($u[$i][2],1)." - ";
					}
					?></td>
	</tr>
	<tr>
		<td width="225" height="100" valign="top">Vos chiens : </td><?php 
					$t = $pdo->mesChiens($_SESSION['id']);
					for ($i=0;$i<count($t);$i++)
					{
						echo "<td><center><img src='images/".$t[$i][2].".jpg' height='100'></center><nobr>";
						echo "<center>".strtoupper(substr($t[$i][1],0,1)).substr($t[$i][1],1)." - ".$t[$i][2]."</center></td>";
					}
					?>
	</tr>
</table>
<br>

<table width="100%">
	<tr>
		<td valign="top">
			<table style="margin-left:75px; margin-right:75px;">
				<form method="POST" action="http://172.17.0.4/projetpio/index.php?uc=inscrireChien">
					<tr><td height="50">Nom du chien : </td><td><input type="text" name="nomchien"><br></td></tr>
					<tr><td height="50">Race : </td><td><select name="race" style="width:173;"><?php 
					$v = $pdo->lesChiens();
					for ($i=0;$i<count($v);$i++)
					{
						echo "<option>".$v[$i][1]."</option>";
					}
					?></select><br></td></tr>
					<tr><td height="50">Date de naissance : </td><td><input type="date" name="datenaisschien" style="width:173; font-family:Arial;"><br></td></tr>
					<tr><td height="50">Numéro de tatouage : </td><td><input type="text" name="tatouage"><br></td></tr>
					<tr><td height="50">Numéro de LOF : </td><td><input type="text" name="lof"><br></td></tr>
					<tr><td colspan="2"><br><br><center><input id="button" type="submit" name="inscrirechien" value="Inscrire ce chien"><br></td></tr>
					</form>
			</table>
		</td>
		<td>
			<table style="margin-left:75px; margin-right:75px;">
			<form method="POST" action="http://172.17.0.4/projetpio/index.php?uc=inscrireFamille">
					<tr><td height="50">Nom : </td><td><input type="text" name="nom"></td></tr>
					<tr><td height="50">Prénom : </td><td><input type="text" name="prenom"></td></tr>
					<tr><td height="50">Date de naissance : </td><td><input type="date" id="date" name="datenaiss" style="width:173; font-family:Arial;"></td></tr>
					<tr><td height="50">Adresse : </td><td><input type="text" name="adresse"></td></tr>
					<tr><td height="50">Numéro de téléphone : </td><td><input type="text" name="tel"></td></tr>
					<tr><td height="50">Adresse e-mail : </td><td><input type="email" name="email"></td></tr>
					<tr><td colspan="2"><br><br><center><input id="button" type="submit" name="inscrirefamille" value="Inscrire ce membre"><br></td></tr>
			</form>
			</table>
		</td>
	</tr>
</table>